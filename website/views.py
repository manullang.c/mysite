from django.shortcuts import redirect, render
from website.models import Item

# Create your views here.

def landing_page(request):
	if request.method == 'POST':
		Item.objects.create(text=request.POST.get('item_text'))
		return redirect('/')
	items = Item.objects.all()
	context = {
        'items': items
    }
	return render(request, 'home.html', context)

def profile_page(request):
	return render(request, 'profile.html')
