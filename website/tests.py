from django.test import TestCase
from website.views import landing_page  
from django.http import HttpRequest
from django.urls import resolve
from website.models import Item

#COPIED FROM FUNCTIONAL TESTS PY
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import unittest
# Create your tests here.


class HomePageTest(TestCase):

    def test_root_url_resolves_to_landing_page_view(self):
        found = resolve('/')  
        self.assertEqual(found.func, landing_page)

    def test_http_response_returns_200(self):
        response = self.client.get('', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_uses_home_template(self):
        response = self.client.get('/') 
        self.assertTemplateUsed(response, 'home.html')

    def test_can_save_a_POST_request(self):
        response = self.client.post('/', data={'item_text': 'A new list item'})

        self.assertEqual(Item.objects.count(), 1)  
        new_item = Item.objects.first()  
        self.assertEqual(new_item.text, 'A new list item')  

    def test_redirects_after_POST(self):
        response = self.client.post('/', data={'item_text': 'A new list item'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')
	
    def test_landing_page_returns_correct_html(self): 
        response = self.client.get('/') 
        html = response.content.decode('utf8')  
        self.assertTrue(html.startswith('<html>'))  
        self.assertIn('<title>Landing Page</title>', html)  
        self.assertTrue(html.endswith('</html>'))

    def test_displays_all_list_items(self):
        Item.objects.create(text='itemey 1')
        Item.objects.create(text='itemey 2')

        response = self.client.get('/')

        self.assertIn('itemey 1', response.content.decode())
        self.assertIn('itemey 2', response.content.decode())

class ItemModelTest(TestCase):

    def test_saving_and_retrieving_items(self):
        first_item = Item()
        first_item.text = 'The first (ever) list item'
        first_item.save()

        second_item = Item()
        second_item.text = 'Item the second'
        second_item.save()

        saved_items = Item.objects.all()
        self.assertEqual(saved_items.count(), 2)

        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]
        self.assertEqual(first_saved_item.text, 'The first (ever) list item')
        self.assertEqual(second_saved_item.text, 'Item the second')

class ProfilePageTest(TestCase):
    def test_http_response_returns_200(self):
        response = self.client.get('/profile/', follow=True)
        self.assertEqual(response.status_code, 200)
    def test_uses_home_template(self):
        response = self.client.get('/profile/') 
        self.assertTemplateUsed(response, 'profile.html')
    def test_landing_page_returns_correct_html(self): 
        response = self.client.get('/profile/') 
        html = response.content.decode('utf8')  
        self.assertTrue(html.startswith('<html>'))  
        self.assertIn('<title>Profile Page</title>', html)
        self.assertIn('<h2 align = center>Profile</h2>', html)
        self.assertTrue(html.endswith('</html>'))


#COPIED FROM FUNCTIONAL TESTS PY
'''
class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()
        super(NewVisitorTest, self).setUp()

    def tearDown(self):
        self.browser.quit()

    def test_css(self):
        self.browser.get('http://ppw-status-staging.herokuapp.com/')
        time.sleep(1) 
        self.assertIn('<h1>',self.browser.page_source)
    def test_css2(self):
        self.browser.get('http://ppw-status-staging.herokuapp.com/')
        time.sleep(1) 
        self.assertIn('<tr>',self.browser.page_source)
    
    def test_layout_title(self):
        self.browser.get('http://ppw-status-staging.herokuapp.com/')
        time.sleep(1)
        # She notices the page header is Landing Page 
        self.assertIn('Landing Page', self.browser.title)
    def test_layout_header(self):
        self.browser.get('http://ppw-status-staging.herokuapp.com/')
        time.sleep(1)
        header_text = self.browser.find_element_by_tag_name('h1').text
        # and the title Hello, Apa kabar?
        self.assertIn('Hello, Apa kabar?', header_text)
    def test_can_start_a_list_and_retrieve_it_later(self):
        # Kon....chi mobil has heard about a cool new local app. She goes
        # to check out its homepage
        self.browser.get('http://ppw-status-staging.herokuapp.com/')
        # She is invited to write a 300 word status. Before she types the 300 word status, a placeholder 'Enter status' is seen. 
        inputbox = self.browser.find_element_by_id('id_new_item')  
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter status'
        )
        # She types'coba coba' into a text box 
        inputbox.send_keys('coba coba')  
        # When she hits the button, the page updates, and now the 300 word essay
        # is available as an item in a status table
        inputbox.send_keys(Keys.ENTER)
        time.sleep(1)  
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')  
        self.assertIn('coba cobal', [row.text for row in rows])
'''
        
